<?php

namespace Flexy\Ftwo\Sdk\Template;

use Flexy\Ftwo\Sdk\Template\Staging\LocalStageWorkingCopyMerger;

/**
 * @package Flexy\Ftwo\Sdk\Template
 */
class TemplateUploader
{

    /**
     * @var TemplateDownloader
     */
    private $downloader;

    /**
     * @var LocalStageWorkingCopyMerger
     */
    private $localWorkingMerger;

    /**
     * @param TemplateDownloader $downloader
     * @param LocalStageWorkingCopyMerger $localWorkingMerger
     */
    public function __construct(
        TemplateDownloader $downloader,
        LocalStageWorkingCopyMerger $localWorkingMerger
    ) {
        $this->downloader = $downloader;
        $this->localWorkingMerger = $localWorkingMerger;
    }

    /**
     *
     */
    public function upload()
    {
        $this->localWorkingMerger->mergeIntoLocalStage();

        $this->downloader->download();
        //use api to upload all store files
    }
}