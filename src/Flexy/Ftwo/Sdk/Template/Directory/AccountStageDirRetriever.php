<?php

namespace Flexy\Ftwo\Sdk\Template\Directory;

use Flexy\Ftwo\Sdk\Command\AccountRetriever;

/**
 * @package Flexy\Ftwo\Sdk\Template\Directory
 */
class AccountStageDirRetriever
{

    /**
     * @var AccountRetriever
     */
    private $accountRetriever;

    /**
     * @var string
     */
    private $stageDir;

    /**
     * @param AccountRetriever $accountRetriever
     * @param $stageDir
     */
    public function __construct(
        AccountRetriever $accountRetriever,
        $stageDir
    ) {
        $this->accountRetriever = $accountRetriever;
        $this->stageDir = $stageDir;
    }

    /**
     * @return string
     */
    public function retrieve()
    {
        return rtrim($this->stageDir, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . $this->accountRetriever->getAccountIdentity();
    }
}