<?php

namespace Flexy\Ftwo\Sdk\Template\Directory;

use Flexy\Ftwo\Sdk\Command\AccountRetriever;

/**
 * @package Flexy\Ftwo\Sdk\Template\Directory
 */
class AccountWorkingDirRetriever
{

    /**
     * @var AccountRetriever
     */
    private $accountRetriever;

    /**
     * @var string
     */
    private $workingDir;

    /**
     * @param AccountRetriever $accountRetriever
     * @param $workingDir
     */
    public function __construct(
        AccountRetriever $accountRetriever,
        $workingDir
    ) {
        $this->accountRetriever = $accountRetriever;
        $this->workingDir = $workingDir;
    }

    /**
     * @return string
     */
    public function retrieve()
    {
        return rtrim($this->workingDir, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . $this->accountRetriever->getAccountIdentity();
    }
}