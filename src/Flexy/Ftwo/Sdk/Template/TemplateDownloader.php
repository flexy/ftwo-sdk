<?php

namespace Flexy\Ftwo\Sdk\Template;

use Flexy\Ftwo\Sdk\Template\Staging\LocalStageRemoteStageMerger;
use Flexy\Ftwo\Sdk\Template\Staging\LocalStageWorkingCopyMerger;
use Flexy\Ftwo\Sdk\Template\Staging\RemoteStageUpdater;

/**
 * @package Flexy\Ftwo\Sdk\Template
 */
class TemplateDownloader
{

    /**
     * @var RemoteStageUpdater
     */
    private $remoteStageUpdater;

    /**
     * @var LocalStageRemoteStageMerger
     */
    private $localRemoteMerger;

    /**
     * @var LocalStageWorkingCopyMerger
     */
    private $localWorkingMerger;

    /**
     * @param RemoteStageUpdater $remoteStageUpdater
     * @param LocalStageRemoteStageMerger $localRemoteMerger
     * @param LocalStageWorkingCopyMerger $localWorkingMerger
     */
    public function __construct(
        RemoteStageUpdater $remoteStageUpdater,
        LocalStageRemoteStageMerger $localRemoteMerger,
        LocalStageWorkingCopyMerger $localWorkingMerger
    ) {
        $this->remoteStageUpdater = $remoteStageUpdater;
        $this->localRemoteMerger = $localRemoteMerger;
        $this->localWorkingMerger = $localWorkingMerger;
    }

    /**
     *
     */
    public function download()
    {
        $this->remoteStageUpdater->update();
        $this->localRemoteMerger->merge();
        $this->localWorkingMerger->mergeIntoWorkingDir();
    }
}