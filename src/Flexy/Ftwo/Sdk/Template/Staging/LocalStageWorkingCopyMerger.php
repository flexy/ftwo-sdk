<?php

namespace Flexy\Ftwo\Sdk\Template\Staging;

use Flexy\Ftwo\Sdk\Template\Directory\AccountStageDirRetriever;
use Flexy\Ftwo\Sdk\Template\Directory\AccountWorkingDirRetriever;

/**
 * @package Flexy\Ftwo\Sdk\Template\Staging
 */
class LocalStageWorkingCopyMerger
{

    /**
     * @var AccountStageDirRetriever
     */
    private $stageDirRetriever;

    /**
     * @var AccountWorkingDirRetriever
     */
    private $workingDirRetriever;

    /**
     * @param AccountStageDirRetriever $stageDirRetriever
     * @param AccountWorkingDirRetriever $workingDirRetriever
     */
    public function __construct(
        AccountStageDirRetriever $stageDirRetriever,
        AccountWorkingDirRetriever $workingDirRetriever
    ) {
        $this->stageDirRetriever = $stageDirRetriever;
        $this->workingDirRetriever = $workingDirRetriever;
    }

    public function mergeIntoLocalStage()
    {
        //copy local files to .ftwo/<store>/local
        //commit
    }

    public function mergeIntoWorkingDir()
    {
        //CUIDADO, pode sobrescrever modificações
        //.ftwo/<store>/local to working dir
    }
}