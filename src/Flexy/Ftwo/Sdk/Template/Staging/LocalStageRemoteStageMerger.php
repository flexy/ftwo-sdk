<?php

namespace Flexy\Ftwo\Sdk\Template\Staging;

use Flexy\Ftwo\Sdk\Template\Directory\AccountStageDirRetriever;

/**
 * @package Flexy\Ftwo\Sdk\Template\Staging
 */
class LocalStageRemoteStageMerger
{

    /**
     * @var AccountStageDirRetriever
     */
    private $stageDirRetriever;

    /**
     * @param AccountStageDirRetriever $stageDirRetriever
     */
    public function __construct(
        AccountStageDirRetriever $stageDirRetriever
    ) {
        $this->stageDirRetriever = $stageDirRetriever;
    }

    public function merge()
    {
        //go to local stage 
        //stash (if stopped in the middle of the process)
        //checkout remote branch
        //pull
        //checkout local branch
        //merge remote branch
    }
}