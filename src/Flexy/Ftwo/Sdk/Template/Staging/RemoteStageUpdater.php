<?php

namespace Flexy\Ftwo\Sdk\Template\Staging;

use Flexy\Ftwo\Sdk\Template\Directory\AccountStageDirRetriever;

/**
 * @package Flexy\Ftwo\Sdk\Template\Staging
 */
class RemoteStageUpdater
{

    /**
     * @var AccountStageDirRetriever
     */
    private $stageDirRetriever;

    /**
     * @param AccountStageDirRetriever $stageDirRetriever
     */
    public function __construct(
        AccountStageDirRetriever $stageDirRetriever
    ) {
        $this->stageDirRetriever = $stageDirRetriever;
    }

    
    public function update()
    {
        //go to .ftwo/<store>/remote
        //use api to download all store files
        //commit
        //push
    }
}