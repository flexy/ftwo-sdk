<?php

namespace Flexy\Ftwo\Sdk\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * @package Flexy\Ftwo\Sdk\Command
 */
abstract class AbstractCommand extends Command implements ContainerAwareInterface
{
    use ContainerAwareTrait;
}