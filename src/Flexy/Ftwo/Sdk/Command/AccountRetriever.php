<?php

namespace Flexy\Ftwo\Sdk\Command;

/**
 * @package Flexy\Ftwo\Sdk\Command
 */
class AccountRetriever
{

    /**
     * @var string
     */
    private $accountIdentity;

    /**
     * @return string
     */
    public function getAccountIdentity()
    {
        return $this->accountIdentity;
    }

    /**
     * @param string $accountIdentity
     */
    public function setAccountIdentity($accountIdentity)
    {
        $this->accountIdentity = $accountIdentity;
    }
}