<?php

namespace Flexy\Ftwo\Sdk\Command\Template;

use Flexy\Ftwo\Sdk\Command\AbstractCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @package Flexy\Ftwo\Sdk\Command\Template
 */
class UploadCommand extends AbstractCommand
{
    protected function configure()
    {
        $this
            ->setName('flexy:sdk:template:upload')
            ->setDescription('Put your local store templates into the remote server')
            ->addArgument(
                'account',
                InputArgument::REQUIRED,
                'Account identity'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->container->get('ftwo_sdk.command.account_retriever')->setAccountIdentity(
            $input->getArgument('account')
        );

        $this->container->get('ftwo_sdk.template.uploader')->upload();
    }
}