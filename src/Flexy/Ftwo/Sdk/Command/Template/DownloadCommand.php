<?php

namespace Flexy\Ftwo\Sdk\Command\Template;

use Flexy\Ftwo\Sdk\Command\AbstractCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @package Flexy\Ftwo\Sdk\Command\Template
 */
class DownloadCommand extends AbstractCommand
{
    protected function configure()
    {
        $this
            ->setName('flexy:sdk:template:download')
            ->setDescription('Download remote templates to your local store')
            ->addArgument(
                'account',
                InputArgument::REQUIRED,
                'Account identity'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->container->get('ftwo_sdk.command.account_retriever')->setAccountIdentity(
            $input->getArgument('account')
        );
        
        $this->container->get('ftwo_sdk.template.downloader')->download();
    }
}